package net.bac.sbe.commons.message.model.fcl;

import static org.junit.Assert.*;

import org.junit.Test;

import net.bac.sbe.commons.message.Message;
import net.bac.sbe.commons.message.model.fcl.EmployerFiveYearsRequest;
import net.bac.sbe.commons.message.model.fcl.EmployerFiveYearsResponse;
import net.bac.sbe.message.utils.MessageParserUtil;

/**
 * @author gdiazs
 * @since 1.0
 */
public class ConsultaPatronosFactoryTest {

	@Test
	public void test() {
		
		Message message = MessageParserUtil.parseMessage(XML_RESPONSE_PLANE_TEXT, EmployerFiveYearsRequest.class, EmployerFiveYearsResponse.class);
		String xmlAsString = MessageParserUtil.parseMessageAsXmlString(message, EmployerFiveYearsRequest.class, EmployerFiveYearsResponse.class);
		System.out.println("Parse String to Message");
		System.out.println(xmlAsString);
		
	}
	
	private static final String XML_RESPONSE_PLANE_TEXT =   "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n" + 
			"<message>\r\n" + 
			"    <header>\r\n" + 
			"        <operationCode>IMCPA</operationCode>\r\n" + 
			"        <origin>\r\n" + 
			"            <channel>EBAC</channel>\r\n" + 
			"            <country>CR</country>\r\n" + 
			"            <server>10.122.91.93</server>\r\n" + 
			"            <user>BCOWEBUSER</user>\r\n" + 
			"        </origin>\r\n" + 
			"        <errors>\r\n" + 
			"            <error>\r\n" + 
			"                <errorCode />\r\n" + 
			"                <errorDescription />\r\n" + 
			"            </error>\r\n" + 
			"        </errors>\r\n" + 
			"    </header>\r\n" + 
			"    <key>\r\n" + 
			"        <CIF>002239084</CIF>\r\n" + 
			"    </key>\r\n" + 
			"    <body>\r\n" + 
			"        <consultaPatronos>\r\n" + 
			"            <CIF>2239084</CIF>\r\n" + 
			"            <nombre>CASTILLO ALEMAN CARLOS ENRIQUE</nombre>\r\n" + 
			"            <idCliente>07-0184-0541</idCliente>\r\n" + 
			"            <tipoIdCliente>0</tipoIdCliente>\r\n" + 
			"            <codFondo>9</codFondo>\r\n" + 
			"            <desFondo>11 - FONDO DE CAPITALIZACION LABORAL - FCL</desFondo>\r\n" + 
			"            <numContrato>NE12010005434</numContrato>\r\n" + 
			"            <codCuenta>695954</codCuenta>\r\n" + 
			"            <valorCuota>0</valorCuota>\r\n" + 
			"            <detallePatronos>\r\n" + 
			"                <patrono>\r\n" + 
			"                    <cedulaJuridica>3101012009</cedulaJuridica>\r\n" + 
			"                    <numPatronal>203101012009001166</numPatronal>\r\n" + 
			"                    <nomPatrono>BANCO BAC SAN JOSE SOCIEDAD ANONIMA</nomPatrono>\r\n" + 
			"                    <saldo>977267.67</saldo>\r\n" + 
			"                    <montoROP>58303.5</montoROP>\r\n" + 
			"                    <montoFCL>918964.17</montoFCL>\r\n" + 
			"                    <montoDispQuinquenio>0</montoDispQuinquenio>\r\n" + 
			"                </patrono>\r\n" + 
			"            </detallePatronos>\r\n" + 
			"        </consultaPatronos>\r\n" + 
			"    </body>\r\n" + 
			"</message>";


}
