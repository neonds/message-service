package net.bac.sbe.message.service.model.message;

import org.junit.Test;

import net.bac.sbe.commons.message.Header;
import net.bac.sbe.commons.message.Message;
import net.bac.sbe.commons.message.Message.MessageBuilder;
import net.bac.sbe.commons.message.Origin;
import net.bac.sbe.commons.message.Target;
import net.bac.sbe.commons.message.model.opc.ConsultaPatronos;
import net.bac.sbe.commons.message.model.opc.DetallePatronos;
import net.bac.sbe.commons.message.model.opc.EmployerFiveYearsRequest;
import net.bac.sbe.commons.message.model.opc.EmployerFiveYearsResponse;
import net.bac.sbe.commons.message.model.opc.Patrono;
import net.bac.sbe.message.utils.MessageParserUtil;

/**
 * @author gdiazs
 * @since 1.0
 */
public class EmployerFiveYearsTest {

	/**
	 * Ejemplo de cómo armar una Trama de consulta
	 * TODO terminar caso de prueba con Assert
	 */
	@Test
	public void employerFiveYearsBuildRequestMessageTest(){
		
		
		MessageBuilder builder = new MessageBuilder();
		
		String result = builder.withHeader(new Header("IMCPA", 
				new Origin("CR", "EBAC", "BCOWEBUSER", "10.122.91.93"), 
				new Target("CR")))
				.withKey(new EmployerFiveYearsRequest("002239084"), EmployerFiveYearsRequest.class)
				.buildXmlString();
			
		System.out.println("Build Request"); 
		System.out.println(result);
	}
	
	
	/**
	 * Ejemplo de cómo construir la Trama respuesta
	 */
	@Test
	public void employerFiveYearsBuildResponseMessageTest(){
		
		
		ConsultaPatronos consultaPatronos = new ConsultaPatronos();
		consultaPatronos.setCIF("2239084");
		consultaPatronos.setCodCuenta("695954");
		consultaPatronos.setCodFondo("9");
		consultaPatronos.setDesFondo("11 - FONDO DE CAPITALIZACION LABORAL - FCL");
		consultaPatronos.setIdCliente("07-0184-0541");
		consultaPatronos.setNumContrato("numContrato");
		consultaPatronos.setValorCuota("0");
		consultaPatronos.setTipoIdCliente("0");
		consultaPatronos.setNombre("CASTILLO ALEMAN CARLOS ENRIQUE");
		
		Patrono patrono = new Patrono();
		patrono.setCedulaJuridica("3101012009");
		patrono.setNumPatronal("203101012009001166");
		patrono.setNomPatrono("BANCO BAC SAN JOSE SOCIEDAD ANONIMA");
		patrono.setSaldo("977267.67");
		patrono.setMontoROP("58303.5");
		patrono.setMontoFCL("918964.17");
		patrono.setMontoDispQuinquenio("0");
		
		DetallePatronos detallePatronos = new DetallePatronos();
		
		detallePatronos.setPatrono(new Patrono[]{patrono});
		
		consultaPatronos.setDetallePatronos( detallePatronos);
		
		EmployerFiveYearsResponse employerFiveYearsResponse = new EmployerFiveYearsResponse();
		employerFiveYearsResponse.setConsultaPatronos(consultaPatronos);
		
		
		MessageBuilder builder = new MessageBuilder();
		
		String result = builder.withHeader(new Header("IMCPA", 
				new Origin("CR", "EBAC", "BCOWEBUSER", "10.122.91.93"), 
				new Target("CR")))
				.withBody(employerFiveYearsResponse, EmployerFiveYearsResponse.class)
				.buildXmlString();
		System.out.println("Build Response");		
		System.out.println(result);
	}
	
	
	/**
	 * Ejemplo de como parsear la respuesta XML de un servicio que devuelve una TRAMA en texto Plano
	 */
	@Test
	public void employerFiveYearsParseMessageFromTextPlane (){
		Message message = MessageParserUtil.parseMessage(XML_RESPONSE_PLANE_TEXT, EmployerFiveYearsRequest.class, EmployerFiveYearsResponse.class);
		
		String xmlAsString = MessageParserUtil.parseMessageAsXmlString(message, EmployerFiveYearsRequest.class, EmployerFiveYearsResponse.class);
		System.out.println("Parse String to Message");
		System.out.println(xmlAsString);
	}
	
	
	
	
	
	
	private static final String XML_RESPONSE_PLANE_TEXT =   "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n" + 
			"<message>\r\n" + 
			"    <header>\r\n" + 
			"        <operationCode>IMCPA</operationCode>\r\n" + 
			"        <origin>\r\n" + 
			"            <channel>EBAC</channel>\r\n" + 
			"            <country>CR</country>\r\n" + 
			"            <server>10.122.91.93</server>\r\n" + 
			"            <user>BCOWEBUSER</user>\r\n" + 
			"        </origin>\r\n" + 
			"        <errors>\r\n" + 
			"            <error>\r\n" + 
			"                <errorCode />\r\n" + 
			"                <errorDescription />\r\n" + 
			"            </error>\r\n" + 
			"        </errors>\r\n" + 
			"    </header>\r\n" + 
			"    <key>\r\n" + 
			"        <CIF>002239084</CIF>\r\n" + 
			"    </key>\r\n" + 
			"    <body>\r\n" + 
			"        <consultaPatronos>\r\n" + 
			"            <CIF>2239084</CIF>\r\n" + 
			"            <nombre>CASTILLO ALEMAN CARLOS ENRIQUE</nombre>\r\n" + 
			"            <idCliente>07-0184-0541</idCliente>\r\n" + 
			"            <tipoIdCliente>0</tipoIdCliente>\r\n" + 
			"            <codFondo>9</codFondo>\r\n" + 
			"            <desFondo>11 - FONDO DE CAPITALIZACION LABORAL - FCL</desFondo>\r\n" + 
			"            <numContrato>NE12010005434</numContrato>\r\n" + 
			"            <codCuenta>695954</codCuenta>\r\n" + 
			"            <valorCuota>0</valorCuota>\r\n" + 
			"            <detallePatronos>\r\n" + 
			"                <patrono>\r\n" + 
			"                    <cedulaJuridica>3101012009</cedulaJuridica>\r\n" + 
			"                    <numPatronal>203101012009001166</numPatronal>\r\n" + 
			"                    <nomPatrono>BANCO BAC SAN JOSE SOCIEDAD ANONIMA</nomPatrono>\r\n" + 
			"                    <saldo>977267.67</saldo>\r\n" + 
			"                    <montoROP>58303.5</montoROP>\r\n" + 
			"                    <montoFCL>918964.17</montoFCL>\r\n" + 
			"                    <montoDispQuinquenio>0</montoDispQuinquenio>\r\n" + 
			"                </patrono>\r\n" + 
			"            </detallePatronos>\r\n" + 
			"        </consultaPatronos>\r\n" + 
			"    </body>\r\n" + 
			"</message>";
}
