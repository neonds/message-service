package net.bac.sbe.commons.message;

import java.util.List;

/**
 * @author gdiazs
 * @since 1.0
 */
public class Errors {
	
	public Errors() {

	}

	List<Error> error;

	/**
	 * @return the errors
	 */
	public List<Error> getError() {
		return error;
	}

	/**
	 * @param errors the errors to set	
	 */
	public void setError(List<Error> errors) {
		this.error = errors;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		String result  = "";
		for (Error error2 : error) {
			result += error2.toString();
		}
		return result;
	}
	
	
	
	
}
