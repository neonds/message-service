package net.bac.sbe.commons.message.model.opc;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author gdiazs
 * @since 1.0
 * IMCPA
 */
@XmlRootElement(name = "key")
@XmlAccessorType(XmlAccessType.FIELD)
public class EmployerFiveYearsRequest {

	private String CIF;
	
	
	
	/**
	 * @param cIF
	 */
	public EmployerFiveYearsRequest(String cIF) {
		super();
		CIF = cIF;
	}

	/**
	 * 
	 */
	public EmployerFiveYearsRequest() {
	}

	/**
	 * @return the cIF
	 */
	public String getCIF() {
		return CIF;
	}

	/**
	 * @param cIF the cIF to set
	 */
	public void setCIF(String CIF) {
		this.CIF = CIF;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "EmployerFiveYearsRequest [CIF=" + CIF + "]";
	}
	
	
	
}

