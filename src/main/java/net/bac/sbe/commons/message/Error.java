package net.bac.sbe.commons.message;

/**
 * @author gdiazs
 * @since 1.0
 */
public class Error {

    private String errorCode;

    private String errorDescription;
    
   

	/**
	 * 
	 */
	public Error() {
	}

	/**
	 * @param errorCode
	 * @param errorDescription
	 */
	public Error(String errorCode, String errorDescription) {
		super();
		this.errorCode = errorCode;
		this.errorDescription = errorDescription;
	}

	/**
	 * @return the errorCode
	 */
	public String getErrorCode() {
		return errorCode;
	}

	/**
	 * @param errorCode the errorCode to set
	 */
	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	/**
	 * @return the errorDescription
	 */
	public String getErrorDescription() {
		return errorDescription;
	}

	/**
	 * @param errorDescription the errorDescription to set
	 */
	public void setErrorDescription(String errorDescription) {
		this.errorDescription = errorDescription;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Error [errorCode=" + errorCode + ", errorDescription=" + errorDescription + "]";
	}
	
	
    
    
}
