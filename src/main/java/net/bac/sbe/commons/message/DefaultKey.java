package net.bac.sbe.commons.message;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * Esta clase se usa cuando no se definen los paramtros en la construcción del mensaje,
 * de esta manera genera un TAG denominado KEY vacío <key />
 * @author gdiazs
 * @since 1.0
 */
@XmlRootElement(name = "key")
public class DefaultKey {

}
