package net.bac.sbe.commons.message;

/**
 * @author gdiazs
 * @since 1.0
 */
public class Origin {

    private String country;

    private String channel;

    private String user;

    private String server;
    
    
    
    

	/**
	 * 
	 */
	public Origin() {
		super();
	}

	/**
	 * @param country
	 * @param channel
	 * @param user
	 * @param server
	 */
	public Origin(String country, String channel, String user, String server) {
		super();
		this.country = country;
		this.channel = channel;
		this.user = user;
		this.server = server;
	}

	/**
	 * @return the country
	 */
	public String getCountry() {
		return country;
	}

	/**
	 * @param country the country to set
	 */
	public void setCountry(String country) {
		this.country = country;
	}

	/**
	 * @return the channel
	 */
	public String getChannel() {
		return channel;
	}

	/**
	 * @param channel the channel to set
	 */
	public void setChannel(String channel) {
		this.channel = channel;
	}

	/**
	 * @return the user
	 */
	public String getUser() {
		return user;
	}

	/**
	 * @param user the user to set
	 */
	public void setUser(String user) {
		this.user = user;
	}

	/**
	 * @return the server
	 */
	public String getServer() {
		return server;
	}

	/**
	 * @param server the server to set
	 */
	public void setServer(String server) {
		this.server = server;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Origin [country=" + country + ", channel=" + channel + ", user=" + user + ", server=" + server + "]";
	}
    
	
	
    
}
