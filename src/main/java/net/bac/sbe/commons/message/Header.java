package net.bac.sbe.commons.message;

import javax.xml.bind.annotation.XmlElement;

/**
 * @author gdiazs
 * @since 1.0
 */
public class Header {

	private String operationCode;

	private Target target;
	
	private Errors errors;

	private Origin origin;

	/**
	 * 
	 */
	public Header() {
	}

	/**
	 * @param operationCode
	 * @param origin
	 */
	public Header(String operationCode, Origin origin, Target target) {
		super();
		this.operationCode = operationCode;
		this.origin = origin;
		this.target = target;
	}

	/**
	 * @return the operationCode
	 */
	public String getOperationCode() {
		return operationCode;
	}

	/**
	 * @param operationCode
	 *            the operationCode to set
	 */
	public void setOperationCode(String operationCode) {
		this.operationCode = operationCode;
	}

	/**
	 * @return the errors
	 */
	public Errors getErrors() {
		return errors;
	}

	/**
	 * @param errors
	 *            the errors to set
	 */
	@XmlElement(name = "errors")
	public void setErrors(Errors errors) {
		this.errors = errors;
	}

	/**
	 * @return the origin
	 */
	public Origin getOrigin() {
		return origin;
	}

	/**
	 * @param origin
	 *            the origin to set
	 */
	public void setOrigin(Origin origin) {
		this.origin = origin;
	}
	
	
	

	/**
	 * @return the target
	 */
	public Target getTarget() {
		return target;
	}

	/**
	 * @param target the target to set
	 */
	@XmlElement(name = "target")
	public void setTarget(Target target) {
		this.target = target;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Header [operationCode=" + operationCode + ", target=" + target + ", errors=" + errors + ", origin="
				+ origin + "]";
	}



}
