package net.bac.sbe.commons.message;

/**
 * @author gdiazs
 * @since 1.0
 */
public class Target {

	private String country;

	
	
	/**
	 * 
	 */
	public Target() {
		super();
	}

	/**
	 * @param country
	 */
	public Target(String country) {
		super();
		this.country = country;
	}

	/**
	 * @return the country
	 */
	public String getCountry() {
		return country;
	}

	/**
	 * @param country the country to set
	 */
	public void setCountry(String country) {
		this.country = country;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Target [country=" + country + "]";
	}
	
	
}
