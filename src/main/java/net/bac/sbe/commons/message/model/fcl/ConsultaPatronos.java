//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML v2.2.8-b130911.1802 
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen. 
// Generado el: 2016.08.23 a las 05:25:05 PM CST 
//


package net.bac.sbe.commons.message.model.fcl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CIF" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="nombre" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="idCliente" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="tipoIdCliente" type="{http://www.w3.org/2001/XMLSchema}byte"/>
 *         &lt;element name="codFondo" type="{http://www.w3.org/2001/XMLSchema}byte"/>
 *         &lt;element name="desFondo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="numContrato" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="codCuenta" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="valorCuota" type="{http://www.w3.org/2001/XMLSchema}byte"/>
 *         &lt;element name="detallePatronos">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="patrono">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="cedulaJuridica" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *                             &lt;element name="numPatronal" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *                             &lt;element name="nomPatrono" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="saldo" type="{http://www.w3.org/2001/XMLSchema}float"/>
 *                             &lt;element name="montoROP" type="{http://www.w3.org/2001/XMLSchema}float"/>
 *                             &lt;element name="montoFCL" type="{http://www.w3.org/2001/XMLSchema}float"/>
 *                             &lt;element name="montoDispQuinquenio" type="{http://www.w3.org/2001/XMLSchema}byte"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "cif",
    "nombre",
    "idCliente",
    "tipoIdCliente",
    "codFondo",
    "desFondo",
    "numContrato",
    "codCuenta",
    "valorCuota",
    "detallePatronos"
})
@XmlRootElement(name = "consultaPatronos")
public class ConsultaPatronos {

    @XmlElement(name = "CIF")
    protected int cif;
    @XmlElement(required = true)
    protected String nombre;
    @XmlElement(required = true)
    protected String idCliente;
    protected byte tipoIdCliente;
    protected byte codFondo;
    @XmlElement(required = true)
    protected String desFondo;
    @XmlElement(required = true)
    protected String numContrato;
    protected int codCuenta;
    protected byte valorCuota;
    @XmlElement(required = true)
    protected ConsultaPatronos.DetallePatronos detallePatronos;

    /**
     * Obtiene el valor de la propiedad cif.
     * 
     */
    public int getCIF() {
        return cif;
    }

    /**
     * Define el valor de la propiedad cif.
     * 
     */
    public void setCIF(int value) {
        this.cif = value;
    }

    /**
     * Obtiene el valor de la propiedad nombre.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * Define el valor de la propiedad nombre.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNombre(String value) {
        this.nombre = value;
    }

    /**
     * Obtiene el valor de la propiedad idCliente.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdCliente() {
        return idCliente;
    }

    /**
     * Define el valor de la propiedad idCliente.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdCliente(String value) {
        this.idCliente = value;
    }

    /**
     * Obtiene el valor de la propiedad tipoIdCliente.
     * 
     */
    public byte getTipoIdCliente() {
        return tipoIdCliente;
    }

    /**
     * Define el valor de la propiedad tipoIdCliente.
     * 
     */
    public void setTipoIdCliente(byte value) {
        this.tipoIdCliente = value;
    }

    /**
     * Obtiene el valor de la propiedad codFondo.
     * 
     */
    public byte getCodFondo() {
        return codFondo;
    }

    /**
     * Define el valor de la propiedad codFondo.
     * 
     */
    public void setCodFondo(byte value) {
        this.codFondo = value;
    }

    /**
     * Obtiene el valor de la propiedad desFondo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDesFondo() {
        return desFondo;
    }

    /**
     * Define el valor de la propiedad desFondo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDesFondo(String value) {
        this.desFondo = value;
    }

    /**
     * Obtiene el valor de la propiedad numContrato.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumContrato() {
        return numContrato;
    }

    /**
     * Define el valor de la propiedad numContrato.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumContrato(String value) {
        this.numContrato = value;
    }

    /**
     * Obtiene el valor de la propiedad codCuenta.
     * 
     */
    public int getCodCuenta() {
        return codCuenta;
    }

    /**
     * Define el valor de la propiedad codCuenta.
     * 
     */
    public void setCodCuenta(int value) {
        this.codCuenta = value;
    }

    /**
     * Obtiene el valor de la propiedad valorCuota.
     * 
     */
    public byte getValorCuota() {
        return valorCuota;
    }

    /**
     * Define el valor de la propiedad valorCuota.
     * 
     */
    public void setValorCuota(byte value) {
        this.valorCuota = value;
    }

    /**
     * Obtiene el valor de la propiedad detallePatronos.
     * 
     * @return
     *     possible object is
     *     {@link ConsultaPatronos.DetallePatronos }
     *     
     */
    public ConsultaPatronos.DetallePatronos getDetallePatronos() {
        return detallePatronos;
    }

    /**
     * Define el valor de la propiedad detallePatronos.
     * 
     * @param value
     *     allowed object is
     *     {@link ConsultaPatronos.DetallePatronos }
     *     
     */
    public void setDetallePatronos(ConsultaPatronos.DetallePatronos value) {
        this.detallePatronos = value;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="patrono">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="cedulaJuridica" type="{http://www.w3.org/2001/XMLSchema}long"/>
     *                   &lt;element name="numPatronal" type="{http://www.w3.org/2001/XMLSchema}long"/>
     *                   &lt;element name="nomPatrono" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="saldo" type="{http://www.w3.org/2001/XMLSchema}float"/>
     *                   &lt;element name="montoROP" type="{http://www.w3.org/2001/XMLSchema}float"/>
     *                   &lt;element name="montoFCL" type="{http://www.w3.org/2001/XMLSchema}float"/>
     *                   &lt;element name="montoDispQuinquenio" type="{http://www.w3.org/2001/XMLSchema}byte"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "patrono"
    })
    public static class DetallePatronos {

        @XmlElement(required = true)
        protected ConsultaPatronos.DetallePatronos.Patrono patrono;

        /**
         * Obtiene el valor de la propiedad patrono.
         * 
         * @return
         *     possible object is
         *     {@link ConsultaPatronos.DetallePatronos.Patrono }
         *     
         */
        public ConsultaPatronos.DetallePatronos.Patrono getPatrono() {
            return patrono;
        }

        /**
         * Define el valor de la propiedad patrono.
         * 
         * @param value
         *     allowed object is
         *     {@link ConsultaPatronos.DetallePatronos.Patrono }
         *     
         */
        public void setPatrono(ConsultaPatronos.DetallePatronos.Patrono value) {
            this.patrono = value;
        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="cedulaJuridica" type="{http://www.w3.org/2001/XMLSchema}long"/>
         *         &lt;element name="numPatronal" type="{http://www.w3.org/2001/XMLSchema}long"/>
         *         &lt;element name="nomPatrono" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="saldo" type="{http://www.w3.org/2001/XMLSchema}float"/>
         *         &lt;element name="montoROP" type="{http://www.w3.org/2001/XMLSchema}float"/>
         *         &lt;element name="montoFCL" type="{http://www.w3.org/2001/XMLSchema}float"/>
         *         &lt;element name="montoDispQuinquenio" type="{http://www.w3.org/2001/XMLSchema}byte"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "cedulaJuridica",
            "numPatronal",
            "nomPatrono",
            "saldo",
            "montoROP",
            "montoFCL",
            "montoDispQuinquenio"
        })
        public static class Patrono {

            protected long cedulaJuridica;
            protected long numPatronal;
            @XmlElement(required = true)
            protected String nomPatrono;
            protected float saldo;
            protected float montoROP;
            protected float montoFCL;
            protected byte montoDispQuinquenio;

            /**
             * Obtiene el valor de la propiedad cedulaJuridica.
             * 
             */
            public long getCedulaJuridica() {
                return cedulaJuridica;
            }

            /**
             * Define el valor de la propiedad cedulaJuridica.
             * 
             */
            public void setCedulaJuridica(long value) {
                this.cedulaJuridica = value;
            }

            /**
             * Obtiene el valor de la propiedad numPatronal.
             * 
             */
            public long getNumPatronal() {
                return numPatronal;
            }

            /**
             * Define el valor de la propiedad numPatronal.
             * 
             */
            public void setNumPatronal(long value) {
                this.numPatronal = value;
            }

            /**
             * Obtiene el valor de la propiedad nomPatrono.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getNomPatrono() {
                return nomPatrono;
            }

            /**
             * Define el valor de la propiedad nomPatrono.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setNomPatrono(String value) {
                this.nomPatrono = value;
            }

            /**
             * Obtiene el valor de la propiedad saldo.
             * 
             */
            public float getSaldo() {
                return saldo;
            }

            /**
             * Define el valor de la propiedad saldo.
             * 
             */
            public void setSaldo(float value) {
                this.saldo = value;
            }

            /**
             * Obtiene el valor de la propiedad montoROP.
             * 
             */
            public float getMontoROP() {
                return montoROP;
            }

            /**
             * Define el valor de la propiedad montoROP.
             * 
             */
            public void setMontoROP(float value) {
                this.montoROP = value;
            }

            /**
             * Obtiene el valor de la propiedad montoFCL.
             * 
             */
            public float getMontoFCL() {
                return montoFCL;
            }

            /**
             * Define el valor de la propiedad montoFCL.
             * 
             */
            public void setMontoFCL(float value) {
                this.montoFCL = value;
            }

            /**
             * Obtiene el valor de la propiedad montoDispQuinquenio.
             * 
             */
            public byte getMontoDispQuinquenio() {
                return montoDispQuinquenio;
            }

            /**
             * Define el valor de la propiedad montoDispQuinquenio.
             * 
             */
            public void setMontoDispQuinquenio(byte value) {
                this.montoDispQuinquenio = value;
            }

        }

    }

}
