package net.bac.sbe.commons.message.model.opc;

/**
 * @author gdiazs
 * @since 1.0
 */
public class Patrono {
	
	private String cedulaJuridica;
	private String numPatronal;
	private String nomPatrono;
	private String saldo;
	private String montoROP;
	private String montoFCL;
	private String montoDispQuinquenio;
	/**
	 * 
	 */
	public Patrono() {
	}
	/**
	 * @return the cedulaJuridica
	 */
	public String getCedulaJuridica() {
		return cedulaJuridica;
	}
	/**
	 * @param cedulaJuridica the cedulaJuridica to set
	 */
	public void setCedulaJuridica(String cedulaJuridica) {
		this.cedulaJuridica = cedulaJuridica;
	}
	/**
	 * @return the numPatronal
	 */
	public String getNumPatronal() {
		return numPatronal;
	}
	/**
	 * @param numPatronal the numPatronal to set
	 */
	public void setNumPatronal(String numPatronal) {
		this.numPatronal = numPatronal;
	}
	/**
	 * @return the nomPatrono
	 */
	public String getNomPatrono() {
		return nomPatrono;
	}
	/**
	 * @param nomPatrono the nomPatrono to set
	 */
	public void setNomPatrono(String nomPatrono) {
		this.nomPatrono = nomPatrono;
	}
	/**
	 * @return the saldo
	 */
	public String getSaldo() {
		return saldo;
	}
	/**
	 * @param saldo the saldo to set
	 */
	public void setSaldo(String saldo) {
		this.saldo = saldo;
	}
	/**
	 * @return the montoROP
	 */
	public String getMontoROP() {
		return montoROP;
	}
	/**
	 * @param montoROP the montoROP to set
	 */
	public void setMontoROP(String montoROP) {
		this.montoROP = montoROP;
	}
	/**
	 * @return the montoFCL
	 */
	public String getMontoFCL() {
		return montoFCL;
	}
	/**
	 * @param montoFCL the montoFCL to set
	 */
	public void setMontoFCL(String montoFCL) {
		this.montoFCL = montoFCL;
	}
	/**
	 * @return the montoDispQuinquenio
	 */
	public String getMontoDispQuinquenio() {
		return montoDispQuinquenio;
	}
	/**
	 * @param montoDispQuinquenio the montoDispQuinquenio to set
	 */
	public void setMontoDispQuinquenio(String montoDispQuinquenio) {
		this.montoDispQuinquenio = montoDispQuinquenio;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Patrono [cedulaJuridica=" + cedulaJuridica + ", numPatronal=" + numPatronal + ", nomPatrono="
				+ nomPatrono + ", saldo=" + saldo + ", montoROP=" + montoROP + ", montoFCL=" + montoFCL
				+ ", montoDispQuinquenio=" + montoDispQuinquenio + "]";
	}
	
	
	
	
	

}
