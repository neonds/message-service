package net.bac.sbe.commons.message.model.opc;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;

/**
 * @author gdiazs
 * @since 1.0
 * IMCPA
 */
@XmlRootElement(name = "body")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlSeeAlso(Patrono.class)
public class EmployerFiveYearsResponse {

	
	
	/**
	 * 
	 */
	public EmployerFiveYearsResponse() {
		super();
	}

	private ConsultaPatronos consultaPatronos;

	/**
	 * @return the consultaPatronos
	 */
	public ConsultaPatronos getConsultaPatronos() {
		return consultaPatronos;
	}

	/**
	 * @param consultaPatronos the consultaPatronos to set
	 */
	public void setConsultaPatronos(ConsultaPatronos consultaPatronos) {
		this.consultaPatronos = consultaPatronos;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "EmployerFiveYearsResponse [consultaPatronos=" + consultaPatronos + "]";
	}
	
	
	
	
	
}
