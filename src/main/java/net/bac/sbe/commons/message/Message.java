package net.bac.sbe.commons.message;

import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAnyElement;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author gdiazs
 * @since 1.0
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public final class Message {

	/**
	 * Tamaño del array de objetos
	 */
	private static final int KEY_AND_BODY_SIZE = 2;

	/**
	 * Indice de la lista de objetos que hace referencia al cuerpo del mensaje
	 */
	private static final int BODY_INDEX = 1;

	/**
	 * Indice de la lista de objetos que hace referencia a la llave del mensaje
	 */
	private static final int KEY_INDEX = 0;
	

	public Message() {
	}

	@XmlElement(name = "header")
	private Header header;

	/**
	 * JAXB interpreta Cualquier clase tipo object en formato array y es capaz de convertirlo a XML
	 * con todos sus propiedades. Cuando se construye un mensaje es más simple para JAXB interpretar el 
	 * array y escribirlos en un XML
	 */
	@XmlAnyElement(lax = true)
	private Object [] keyAndBody;


	/**
	 * @return the header
	 */
	public Header getHeader() {
		return header;
	}

	/**
	 * @param header
	 *            the header to set
	 */
	public void setHeader(Header header) {
		this.header = header;
	}



	/**
	 * @return the keyAndBody
	 */
	public Object [] getKeyAndBody() {
		return keyAndBody;
	}

	/**
	 * @param keyAndBody the keyAndBody to set
	 */
	public void setKeyAndBody(Object []  keyAndBody) {
		this.keyAndBody = keyAndBody;
	}
	
	
	public Object getBody(){
		return keyAndBody[BODY_INDEX]; 
	}
	

	
	public Object getKey(){
		return keyAndBody[KEY_INDEX];
	}


	/**
	 * @author gdiazs
	 * @since 1.0
	 */
	public static class MessageBuilder {

		private Header header;

		private Object body;

		private List<Error> errors;

		private Object key;

		@SuppressWarnings("rawtypes")
		private Class bodyClass;
		
		@SuppressWarnings("rawtypes")
		private Class keyClass;

		public MessageBuilder() {

		}

		/**
		 * @param header
		 * @return
		 */
		public MessageBuilder withHeader(Header header) {
			this.header = header;
			return this;
		}

		/**
		 * @param body
		 * @return
		 */
		@SuppressWarnings("rawtypes")
		public MessageBuilder withBody(Object body, Class bodyClass) {
			this.body = body;
			this.bodyClass = bodyClass;
			return this;

		}

		/**
		 * @param error
		 * @return
		 */
		public MessageBuilder withError(Error error) {
			if (errors == null)
				errors = new ArrayList<Error>();

			errors.add(error);

			return this;
		}

		/**
		 * @param key
		 * @return
		 */
		@SuppressWarnings("rawtypes")
		public MessageBuilder withKey(Object key, Class keyClass) {
			this.key = key;
			this.keyClass = keyClass;
			return this;
		}

		public Message build() {
			if (header == null)
				throw new IllegalArgumentException("Header cannot be null");

			if (body == null){
				DefaultBody defaultBody = new DefaultBody();
				body = defaultBody;
				this.bodyClass = DefaultBody.class;
			}
				

			if (key == null){
				DefaultKey defaultKey = new DefaultKey();
				key = defaultKey;
				this.keyClass = DefaultKey.class;
			}
				


			Message message = new Message();
			Object [] keyAndBody = new Object[KEY_AND_BODY_SIZE];
			keyAndBody[KEY_INDEX] = key;
			keyAndBody[BODY_INDEX] = body;
			
			message.setKeyAndBody(keyAndBody);
			Errors errors = new Errors();
			errors.setError(this.errors);
			header.setErrors(errors);
			message.setHeader(header);
			
			

			return message;
		}

		public String buildXmlString() {
			Message message = build();
			


			// Crea una instancia jaxb con la definición del manesaje
			JAXBContext jaxbContext;

			String result = "";
			try {

				StringWriter writer = new StringWriter();
				jaxbContext = JAXBContext.newInstance(Message.class, keyClass, bodyClass);
				Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

				// Identa el XML
				jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

				// Imprime en pantalla el xml
				jaxbMarshaller.marshal(message, writer);
				result = writer.toString();
			} catch (JAXBException e) {
				e.printStackTrace();
			}

			return result;

		}
	}



	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Message [header=" + header + ", key=" + getKey() + ", body= " + getBody() +  "]";
	}
}
