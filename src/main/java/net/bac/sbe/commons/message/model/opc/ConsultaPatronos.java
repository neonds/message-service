package net.bac.sbe.commons.message.model.opc;

/**
 * @author gdiazs
 * @since 1.0
 */
public class ConsultaPatronos {

	private String CIF;
	private String nombre;
	private String idCliente;
	private String tipoIdCliente;
	private String codFondo;
	private String desFondo;
	private String numContrato;
	private String codCuenta;
	private String valorCuota;

	private DetallePatronos detallePatronos;

	/**
	 * 
	 */
	public ConsultaPatronos() {
	}

	/**
	 * @return the cIF
	 */
	public String getCIF() {
		return CIF;
	}

	/**
	 * @param cIF
	 *            the cIF to set
	 */
	public void setCIF(String CIF) {
		this.CIF = CIF;
	}

	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * @param nombre
	 *            the nombre to set
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/**
	 * @return the idCliente
	 */
	public String getIdCliente() {
		return idCliente;
	}

	/**
	 * @param idCliente
	 *            the idCliente to set
	 */
	public void setIdCliente(String idCliente) {
		this.idCliente = idCliente;
	}

	/**
	 * @return the tipoIdCliente
	 */
	public String getTipoIdCliente() {
		return tipoIdCliente;
	}

	/**
	 * @param tipoIdCliente
	 *            the tipoIdCliente to set
	 */
	public void setTipoIdCliente(String tipoIdCliente) {
		this.tipoIdCliente = tipoIdCliente;
	}

	/**
	 * @return the codFondo
	 */
	public String getCodFondo() {
		return codFondo;
	}

	/**
	 * @param codFondo
	 *            the codFondo to set
	 */
	public void setCodFondo(String codFondo) {
		this.codFondo = codFondo;
	}

	/**
	 * @return the desFondo
	 */
	public String getDesFondo() {
		return desFondo;
	}

	/**
	 * @param desFondo
	 *            the desFondo to set
	 */
	public void setDesFondo(String desFondo) {
		this.desFondo = desFondo;
	}

	/**
	 * @return the numContrato
	 */
	public String getNumContrato() {
		return numContrato;
	}

	/**
	 * @param numContrato
	 *            the numContrato to set
	 */
	public void setNumContrato(String numContrato) {
		this.numContrato = numContrato;
	}

	/**
	 * @return the codCuenta
	 */
	public String getCodCuenta() {
		return codCuenta;
	}

	/**
	 * @param codCuenta
	 *            the codCuenta to set
	 */
	public void setCodCuenta(String codCuenta) {
		this.codCuenta = codCuenta;
	}

	/**
	 * @return the valorCuota
	 */
	public String getValorCuota() {
		return valorCuota;
	}

	/**
	 * @param valorCuota
	 *            the valorCuota to set
	 */
	public void setValorCuota(String valorCuota) {
		this.valorCuota = valorCuota;
	}

	/**
	 * @return the detallePatronos
	 */
	public DetallePatronos getDetallePatronos() {
		return detallePatronos;
	}

	/**
	 * @param detallePatronos
	 *            the detallePatronos to set
	 */
	public void setDetallePatronos(DetallePatronos detallePatronos) {
		this.detallePatronos = detallePatronos;
	}

}
