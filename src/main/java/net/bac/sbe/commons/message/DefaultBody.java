package net.bac.sbe.commons.message;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author gdiazs
 * @since 1.0
 */
@XmlRootElement(name = "body")
public class DefaultBody {

}
