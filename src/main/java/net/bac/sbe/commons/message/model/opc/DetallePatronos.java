package net.bac.sbe.commons.message.model.opc;

import javax.xml.bind.annotation.XmlElement;

/**
 * @author gdiazs
 * @since 1.0
 */
public class DetallePatronos {

	@XmlElement
	private Patrono [] patrono;

	/**
	 * @return the patronos
	 */
	public Patrono[] getPatronos() {
		return patrono;
	}

	/**
	 * @param patronos the patronos to set
	 */
	public void setPatrono(Patrono[] patrono) {
		this.patrono = patrono;
	}
	
	
}
