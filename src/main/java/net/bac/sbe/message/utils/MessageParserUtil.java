package net.bac.sbe.message.utils;

import java.io.StringReader;
import java.io.StringWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import net.bac.sbe.commons.message.Message;

/**
 * @author gdiazs
 * @since 1.0
 */
public class MessageParserUtil {

	/**
	 * Convierte un Xml en objeto Mensaje NOTA: Los objetos tipo clase deben ser
	 * aquellas clases a mapear con contenido del Key y Body del mensaje
	 * 
	 * @param xmlString
	 *            Definición del Xml en texto
	 * @param keyClass
	 *            Clase Key definida
	 * @param bodyClass
	 *            Clase Body definida
	 * @return Una instancia del objeto Message con el contenido del XML
	 */

	@SuppressWarnings("rawtypes")
	public static Message parseMessage(String xmlString, Class keyClass, Class bodyClass) {
		Message message = null;
		try {
			JAXBContext jaxbContext = JAXBContext.newInstance(Message.class, keyClass, bodyClass);
			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();

			StringReader reader = new StringReader(xmlString);
			message = (Message) jaxbUnmarshaller.unmarshal(reader);
		} catch (JAXBException e) {
			e.printStackTrace();
		}

		return message;
	}

	/**
	 * Convierte un Objeto Message a una cadena de String en formato XML
	 * 
	 * @param message
	 * @return
	 */
	public static String parseMessageAsXmlString(Message message, Class keyClass, Class bodyClass) {

		// Crea una instancia jaxb con la definición del manesaje
		JAXBContext jaxbContext;

		String result = "";
		try {

			StringWriter writer = new StringWriter();
			jaxbContext = JAXBContext.newInstance(Message.class, keyClass, bodyClass);
			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
			// Identa el XML
			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

			// Imprime en pantalla el xml
			jaxbMarshaller.marshal(message, writer);
			result = writer.toString();
		} catch (JAXBException e) {
			e.printStackTrace();
		}

		return result;

	}

}
